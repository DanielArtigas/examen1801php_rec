<?php
/**
*
*/
class App
{

    function __construct()
    {
        session_start();
        $this->listas = [
            "Juan",
            "Ana",
            "Pedro",
            "Marcelo",
            "Sergio"
        ];
        if (!isset($_SESSION['listas'])) {
            $_SESSION['listas'] = [];
        }
    }

    public function lista1(){

        require 'lista1.php';
    }

    public function lista2(){

       $_SESSION['name'] = $_REQUEST['name'];
       require 'lista2.php';
   }

   public function lista3() {

       $listas = $this->listas;
       require 'lista3.php';
   }

   public function lista4(){

       $_SESSION['listas'] = $_REQUEST['listas'];
       require 'lista4.php';
   }

   public function fin()
   {
       unset($_SESSION);
       session_destroy();
       header('Location:/lista');
   }

    public function delete(){
        //echo "Delete $_REQUEST[key]!!";
        $key = (integer) $_REQUEST['key'];
        unset($_SESSION['listas'][$key]);
        header('Location:index.php?method=lista4');
    }
}
