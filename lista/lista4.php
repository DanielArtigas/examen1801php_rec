<!DOCTYPE html>
<html>
<head>
   <title>Lista</title>
</head>
<body>
   <h1>Lista de Usuarios. Información completa</h1>

   <h4>Datos personales</h4>
   <ul>
       <li>
           Nombre: <?php echo $_SESSION['name'] ?>
       </li>
   </ul>
   <a href="?">Modificar</a>
   <h4> Lista:</h4>
   <ul>
       <?php foreach ($_SESSION['listas'] as $lista): ?>
           <li><?php echo $lista ?></li>
       <?php endforeach ?>
   </ul>
   <a href="?method=lista3">Modificar</a>

   <hr>

   <a href="?method=fin">Iniciar nuevo registro</a>
</body>
</html>
