# Ejercicios de introduccion PHP

## Empezar

- Haz un fork de este repositorio
- Clona en tu equipo el repositorio que acabas de crear.

## Configuración de apache

- Copia el fichero de configuración suministrado en /etc/apache2/sites-available/
- Para copiarlo usar sudo:

```
sudo cp ejerciciosphp.conf /etc/apache2/sites-available/
```

- Activa el nuevo sitio web: `sudo a2ensite ejerciciosphp.conf`

- Reinicia apache: `sudo service apache2 restart`


## Resolución de nombres


 - Abre con sudo el fichero /etc/hosts y añade la línea:

```
127.0.0.1   ejerciciosphp.local
```

## Comprobación

- Abre en tu navegador `http://ejerciciosphp.local`



## Enunciado

Debes  construir una lista de nombres en una variable de sesión llamada "lista".


Debes usar POO


-    En PHP debes usar una única clase App

-    En Java EE puedes usar un único servlet o varios (uno por método).


Tu aplicación debe responder a los siguientes metodos:


-    "index" para mostrar la lista.
        La lista en sí está dentro de un formulario (id="form1") y cada elemento está asociado a un checkbox.

-    "add" para añadir un elemento a la lista. Es el "action" de un formulario colocado al pie de la lista (id="form2").
-    "delete" para eliminar elementos de la lista. Se eliminan los checkbox seleccionados.



<hr>
Notas PHP:

-    Usar POO
-    Debes usar una sola clase.
-    Separar lógica de vista. Ningún "echo" en la clase.


Notas Java:

-    Usar uno o más servlets
-   Las vistas se construyen con ficheros jsp "ocultos".
